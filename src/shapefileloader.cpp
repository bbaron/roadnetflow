#include "shapefileloader.h"

#include <QString>
#include <QSet>
#include <QGraphicsItemGroup>
#include <QDebug>

ShapefileLoader::~ShapefileLoader()
{
}

QGraphicsItemGroup * ShapefileLoader::drawShapefile()
{
    // TODO Remove the previous group created
    _shapefileGraphic = new QGraphicsItemGroup();
    int size = 1;
    QColor color = QColor(0,0,255);
    QPen pen = QPen(QColor(255,0,0));
    pen.setWidth(1);
    pen.setCosmetic(true);

    foreach (auto geom, _shapes) {
        if(wkbFlatten(geom->getGeometryType()) == wkbLineString) {
            OGRLineString * ls = (OGRLineString *) geom;
            QPainterPath path;
            OGRPoint pt;
            ls->getPoint(0,&pt);
            path.moveTo(QPointF(pt.getX(), pt.getY()));
            for(int i = 1; i < ls->getNumPoints(); ++i) {
                ls->getPoint(i,&pt);
                path.lineTo(QPointF(pt.getX(), pt.getY()));
            }
            QGraphicsPathItem * item = new QGraphicsPathItem(path);
            pen.setColor(color);
            item->setPen(pen);
            _shapefileGraphic->addToGroup(item);
        }
        else if(wkbFlatten(geom->getGeometryType()) == wkbPoint) {
            OGRPoint * pt = (OGRPoint *) geom;
            QGraphicsEllipseItem * item = new QGraphicsEllipseItem(pt->getX()-size/2.0, pt->getY()-size/2.0, size, size);
            item->setBrush(QBrush(color));
            item->setPen(Qt::NoPen);
            _shapefileGraphic->addToGroup(item);
        }
    }

    return _shapefileGraphic;
}


void ShapefileLoader::load()
{
    _loadResult = QtConcurrent::run(this, &ShapefileLoader::concurrentLoad);
}

bool ShapefileLoader::concurrentLoad()
{
    OGRRegisterAll();
    OGRDataSource *poDS;

    poDS = OGRSFDriverRegistrar::Open(_filename.toStdString().c_str(), FALSE);
    if( poDS == NULL )
    {
       qWarning() << "Open failed for " << _filename;
       return false;
    }

    _shapes.clear();

    // Set the accepted featue fields
    QSet<QString> acceptedHWFieldsSet = QSet<QString>();
    acceptedHWFieldsSet << "primary_link" << "tertiary_link" << "trunk_link" << "motorway" /*<< "road" */ <<  "secondary_link" << "tertiary" << "motorway_link" << "secondary" << "trunk" << "primary";

    OGRSpatialReference *poTarget = new OGRSpatialReference();
    poTarget->importFromProj4(_projOut.toLatin1().data());
    qDebug() << _projOut;

    for(int i = 0; i < poDS->GetLayerCount(); ++i) {
       OGRLayer  *poLayer = poDS->GetLayer(i);
       qDebug() << "Loading layer" << QString::fromStdString(poLayer->GetName()) << "...";
       OGRFeature *poFeature;

       poLayer->ResetReading();
       OGRFeatureDefn *poFDefn = poLayer->GetLayerDefn();
       int hwIdx = poFDefn->GetFieldIndex("type");
       if(hwIdx == -1)
           hwIdx = poFDefn->GetFieldIndex("highway");

       qDebug() << "highway field index" << hwIdx;
       int nrofFeatures = 0;
       while( (poFeature = poLayer->GetNextFeature()) != NULL )
       {
           QString HWFieldStr = QString::fromStdString(poFeature->GetFieldAsString(hwIdx));
           if(acceptedHWFieldsSet.contains(HWFieldStr)) {
               OGRGeometry *poGeometry = poFeature->GetGeometryRef();
               if( poGeometry != NULL)
                {
                   poGeometry->transformTo(poTarget);
                   OGRPoint * pt = (OGRPoint *) poGeometry;
                   _shapes.append(pt);

               }
               else if (poGeometry != NULL
                        && wkbFlatten(poGeometry->getGeometryType()) == wkbLineString)
               {
                   poGeometry->transformTo(poTarget);
                   OGRLineString * ls = (OGRLineString *) poGeometry;
                   _shapes.append(ls);

               }
           }
           if(nrofFeatures % 100 == 0)
           {
               qreal loadProgress = nrofFeatures / (qreal) poLayer->GetFeatureCount();
               emit loadProgressChanged(loadProgress);
           }
           nrofFeatures++;
       }
    }

    emit loadProgressChanged((qreal)1.0);
    // Do not delete any structure as they will be used later
    qDebug() << "loaded shapefile" << QFileInfo(_filename).fileName() << "with" << _shapes.count() << "features";
    return true;
}
