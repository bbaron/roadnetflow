#-------------------------------------------------
#
# Project created by QtCreator 2015-09-18T14:39:23
#
#-------------------------------------------------

QT       += core gui concurrent opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RoadNetFlow
TEMPLATE = app

CONFIG   += c++11

QMAKE_MAC_SDK = macosx10.11

SOURCES += main.cpp\
    mainwindow.cpp \
    netloader.cpp \
    progressdialog.cpp \
    shapefileloader.cpp \
    roadnet.cpp

HEADERS  += mainwindow.h \
    customscene.h \
    graphicsview.h \
    netloader.h \
    constants.h \
    progressdialog.h \
    shapefileloader.h \
    loader.h \
    graphicsitem.h \
    roadnet.h

FORMS    += mainwindow.ui \
    progressdialog.ui

QMAKE_CXXFLAGS = -mmacosx-version-min=10.8 -std=gnu0x -stdlib=libc+

unix|win32: LIBS += -L$$PWD/../../../../../../../usr/local/Cellar/proj/4.9.1/lib/ -lproj.9

INCLUDEPATH += $$PWD/../../../../../../../usr/local/Cellar/proj/4.9.1/include
DEPENDPATH += $$PWD/../../../../../../../usr/local/Cellar/proj/4.9.1/include


unix|win32: LIBS += -L$$PWD/../../../../../../../usr/local/Cellar/geos/3.4.2/lib/ -lgeos-3.4.2

INCLUDEPATH += $$PWD/../../../../../../../usr/local/Cellar/geos/3.4.2/include
DEPENDPATH += $$PWD/../../../../../../../usr/local/Cellar/geos/3.4.2/include


unix|win32: LIBS += -L$$PWD/../../../../../../../usr/local/Cellar/gdal/1.11.2_1/lib/ -lgdal.1

INCLUDEPATH += $$PWD/../../../../../../../usr/local/Cellar/gdal/1.11.2_1/include
DEPENDPATH += $$PWD/../../../../../../../usr/local/Cellar/gdal/1.11.2_1/include
