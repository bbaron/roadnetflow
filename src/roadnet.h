#ifndef ROADNET_H
#define ROADNET_H

#include <QHash>
#include <QGraphicsItem>


class RoadLink
{
public:
    RoadLink(QString id, QString description, double x1, double y1, double x2, double y2):
    _id(id), _description(description), _p1(x1,y1), _p2(x2,y2) {}

    QString toString() {
        return "id: " + _id +
               " (" + QString::number(_p1.x()) +
               ", " + QString::number(_p1.y()) +
               ") -> (" + QString::number(_p2.x()) +
               ", " + QString::number(_p2.y()) +
               ")";
    }

    QLineF getLine() {
        return QLineF(_p1.x(), _p1.y(), _p2.x(), _p2.y());
    }

private:
    QPointF _p1, _p2;
    QString _id, _description;
};


class RoadNet
{
public:
    RoadNet();
    ~RoadNet();
    QList<QGraphicsItem*> drawNetwork();
    void setVisible(bool visible);
    void addLink(QString id, QString description, double x1, double y1, double x2, double y2);
    bool isEmpty();

private:
    QHash<QString, RoadLink*> _roadLinks;
    QList<QGraphicsItem*> _roadLinksGraphics;

};

#endif // ROADNET_H
