import xlrd
import csv
import sys
import optparse

def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("-i",  action="store", dest='xls_file', help="XLS input file")
    optParser.add_option("-s",  action="store", dest='xls_sheet_name', help='XLS imput file sheet')
    optParser.add_option("-o",  action="store", dest='csv_file', help='CSV output file')

    options, args = optParser.parse_args()
    return options

def xls_to_csv(xls_file, xls_sheet_name, csv_file):
   wb = xlrd.open_workbook(xls_file)
   sh = wb.sheet_by_name(xls_sheet_name)
   cv = open(csv_file, 'wb')
   wr = csv.writer(cv, quoting=csv.QUOTE_NONE)

   for rownum in xrange(sh.nrows):
       wr.writerow(sh.row_values(rownum))

   cv.close()

if __name__ == '__main__':
    options = get_options()
    xls_to_csv(options.xls_file, options.xls_sheet_name, options.csv_file)
