#ifndef GRAPHICSITEM_H
#define GRAPHICSITEM_H

#include <QGraphicsLineItem>
#include <QPen>
#include <QPainter>
#include <cmath>

using namespace std;

class RoadLinkItem : public QGraphicsLineItem
{
    // taken from http://www.walletfox.com/course/customqgraphicslineitem.php
public:
    RoadLinkItem(QLineF line, QGraphicsItem* parent = 0):
            QGraphicsLineItem(line, parent), _selectionOffset(20)
    {
        setFlags(QGraphicsItem::ItemIsSelectable);
        createSelectionPolygon();
    }

    QRectF boundingRect() const
    {
        return _selectionPolygon.boundingRect();
    }
    QPainterPath shape() const
    {
        QPainterPath ret;
        ret.addPolygon(_selectionPolygon);
        return ret;
    }
    void paint (QPainter* painter, const QStyleOptionGraphicsItem* option,
                QWidget* widget = 0)
    {
        Q_UNUSED(option);
        Q_UNUSED(widget);
        QPen pen;
        pen.setCosmetic(true);
        pen.setStyle(Qt::SolidLine);
        if (isSelected()) {
            pen.setColor(Qt::red);
            pen.setWidth(3);
//            painter->drawPolygon(_selectionPolygon);
        } else {
            pen.setColor(Qt::black);
            pen.setWidth(2);
        }
        painter->setPen(pen);
        painter->drawLine(line());
    }

private:
    qreal _selectionOffset;
    QPolygonF _selectionPolygon;
    void createSelectionPolygon() {
        QPolygonF nPolygon;
        qreal pi = 3.141592653589793238463;
        qreal radAngle = line().angle()* pi / 180;
        qreal dx = _selectionOffset * sin(radAngle);
        qreal dy = _selectionOffset * cos(radAngle);
        QPointF offset1 = QPointF(dx, dy);
        QPointF offset2 = QPointF(-dx, -dy);
        nPolygon << line().p1() + offset1
                 << line().p1() + offset2
                 << line().p2() + offset2
                 << line().p2() + offset1;
        _selectionPolygon = nPolygon;
        update();
    }
};

#endif // GRAPHICSITEM_H

