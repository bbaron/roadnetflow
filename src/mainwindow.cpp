#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "graphicsview.h"
#include "progressdialog.h"
#include "constants.h"

#include <QFileDialog>
#include <QGLWidget>
#include <QGLFormat>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _scene = new CustomScene();
    GraphicsView* v = new GraphicsView(_scene, this);
    setCentralWidget(v);

    //to enable OpenGL rendering
    v->setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));

    connect(ui->actionLoad_Flow_file, SIGNAL(triggered(bool)), this, SLOT(openFlowFile()));
    connect(ui->actionLoad_Net_file, SIGNAL(triggered(bool)), this, SLOT(openNetFile()));
    connect(ui->actionLoad_Shapefile, SIGNAL(triggered(bool)), this, SLOT(openShapefile()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openNetFile()
{
    QSettings settings;
    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Open a Network file",
                                                    settings.value("defaultNetFilePath", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString(),
                                                    tr("Net file (*.csv *.txt);;All files (*.*)"));
    if(filename.isEmpty()) {
        return;
    }
    // Save the filename path in the app settings
    settings.setValue("defaultNetFilePath", QFileInfo(filename).absolutePath());

    _netLoader = new NetLoader(this, filename, ",", true);
    ProgressDialog* progressDiag = new ProgressDialog(this, "Loading "+QFileInfo(filename).fileName());
    connect(_netLoader, &NetLoader::loadProgressChanged, progressDiag, &ProgressDialog::updateProgress);
    _netLoader->load();
    progressDiag->exec();

    // draw the road network on the scene
    QList<QGraphicsItem*> groupItem = _netLoader->drawNetwork();
    _scene->addItems(groupItem);
    _showRoadNet = true;

    QMenu* menu = new QMenu();
    menu->setTitle("Road network");
    QAction* actionOpen_showRoadNet = menu->addAction("Show Road network");
    connect(actionOpen_showRoadNet, &QAction::triggered, [=]() {
        _showRoadNet = !_showRoadNet;
        _netLoader->setVisible(_showRoadNet);
    });

    ui->menuBar->addMenu(menu);
}

void MainWindow::openFlowFile()
{

}

void MainWindow::openShapefile()
{
    QSettings settings;
    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Open a Shapefile",
                                                    settings.value("defaultShapefilePath", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString(),
                                                    tr("Shapefile (*.shp)"));
    if(filename.isEmpty()) {
        return;
    }
    // Save the filename path in the app settings
    settings.setValue("defaultShapefilePath", QFileInfo(filename).absolutePath());

    _shapefileLoader = new ShapefileLoader(this, filename, PROJ_IN, PROJ_OUT);
    ProgressDialog* progressDiag = new ProgressDialog(this, "Loading "+QFileInfo(filename).fileName());
    connect(_shapefileLoader, &ShapefileLoader::loadProgressChanged, progressDiag, &ProgressDialog::updateProgress);
    _shapefileLoader->load();
    progressDiag->exec();

    // Draw the shape on the scene
    QGraphicsItemGroup* groupItem = _shapefileLoader->drawShapefile();
    _scene->addItem(groupItem);
    _showShapefile = true;

    QMenu* menu = new QMenu();
    menu->setTitle("Shapefile");
    QAction* actionOpen_showShapefile = menu->addAction("Show shapefile");
    connect(actionOpen_showShapefile, &QAction::triggered, [=]() {
        _showShapefile = !_showShapefile;
        _shapefileLoader->setVisible(_showShapefile);
    });
//    connect(actionOpen_showShapefile, SIGNAL(triggered(bool)), this, SLOT(showShapefile()));

    ui->menuBar->addMenu(menu);
}

